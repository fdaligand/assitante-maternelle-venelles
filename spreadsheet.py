import gspread
from oauth2client.service_account import ServiceAccountCredentials



class GDriveSpreadSheet:
    """
    Export google sheet from your drive.
    Use gspread librabry to get/update/delete data from a google spread sheet
    """

    def __init__(self, sheet_name):
        # follow this tutorial to setup api access coreeclty
        self.sheet_name = sheet_name
        self.sheet = None

        scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
        self.creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
        # use creds to create a client to interact with the Google Drive API
        self.client = gspread.authorize(self.creds)

    def get_all_records_from_sheet(self, number=1):
        """
        Return all datas contains on the specified sheet
        :param number: sheet number
        :return: dictionnary containing all datas
        """
        self.sheet = self.client.open(self.sheet_name).sheet1
        return self.sheet.get_all_records()
