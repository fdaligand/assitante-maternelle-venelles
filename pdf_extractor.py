import csv
import webbrowser
from dataclasses import dataclass

import folium
import requests
import logging
from collections import namedtuple
from spreadsheet import GDriveSpreadSheet
logging.basicConfig(level=logging.DEBUG)


sanitized_location_word = {
    'IMP': 'IMPASSE',
    'LOT': '',
    'PL': 'PLACE',
    'CHE': 'CHEMIN',
    'ALL': 'ALLEE',
    'RTE': 'ROUTE'
}

excluded_location_words = ['BAT', 'RESIDENCE', 'LE HAMEAU DU COTEAU SUD']

@dataclass
class AssMat:
    full_name: str
    address: str
    contact: str
    agrement: str
    phoned: bool
    disponibility: bool
    notes: str

    def __post_init__(self):
        self.phoned = True if self.phoned == 'TRUE' else False
        self.disponibility = True if self.disponibility == 'TRUE' else False
        # append city name and zip code on adress
        self.address += '\n13770 venelles'
        self.sanitize_address()

    def sanitize_address(self):
        """
        Somentimes, word on address are shortened or misspelled.
        it result on emtpy result when we search for coordiante.
        Try to fix it in this function by modify in place ass_mat address
        :param ass_mat:
        """

        # remove to specific address location
        self.address = self.remove_to_specific_location_from_address()
        tokenize_address = self.address.replace('\n', ' ').split(' ')
        for token in tokenize_address:
            if token.upper() in sanitized_location_word:
                self.address = self.address.replace(token, sanitized_location_word[token])

    def remove_to_specific_location_from_address(self):
        """
        Address including to precise location (batiment c1) generate empty result when searching for coordinate.
        :return: string representing a more general address
        """

        general_address = []
        for address_line in self.address.split('\n'):
            to_specific = False
            for word in excluded_location_words:
                if word in address_line:
                    logging.warning(f'forbidden word {word} find in {self.address}, remove it')
                    to_specific = True
            if not to_specific:
                general_address.append(address_line)

        return '\n'.join(general_address)

    def chain_address_with_delimitor(self, delimitor=' '):
        """generate string where all space char (' ') on address attribute are replace by delimitor"""

        return delimitor.join(self.address.split(' '))





class AssMatMap:
    """ Build a maps according to ass mat datas"""

    AssMat = namedtuple('AssMat', 'full_name, address, contact, agrement, phoned, notes ')

    def __init__(self):

        self.maps = folium.Map(location=[43.6, 5.4833], zoom_start=15)
        spread_sheet = GDriveSpreadSheet("liste assmat venelles ")
        self.all_records = spread_sheet.get_all_records_from_sheet()
        self.total_ass_mat = 0
        self.ass_mat_localized = 0
        self.markers = []

    @staticmethod
    def build_popup_message(ass_mat):
        """Generate popup string associated to an ass mat location"""

        popup_msg = f"""
        <p>name : {ass_mat.full_name}<p>
        <p>contact : {ass_mat.contact}<p>
        <p>appelé: {"OUI" if ass_mat.phoned else "NON"}<p>
        """

        return popup_msg

    def choose_icon(self, ass_mat):

        if not ass_mat:
            return None

        if not ass_mat.phoned:
            return folium.Icon(color='blue')
        else:
            if not ass_mat.disponibility:
                return folium.Icon(color='red')
            else:
                return folium.Icon(color='green')

        return None



    def get_latitude_and_longitude_from_adress(self, ass_mat):
        """
        :param ass_mat: information on ass mat to lacte
        :return: list [latitude, longitude]
        """
        query = ass_mat.chain_address_with_delimitor(delimitor='+')
        resp = requests.get(f"https://nominatim.openstreetmap.org/search?q={query}&format=json")
        if resp.ok and resp.json():
            self.ass_mat_localized += 1
            adress_map_information = resp.json()[0]
            return [adress_map_information['lat'], adress_map_information['lon']]
        else:
            logging.warning(f"no position found on map for address {ass_mat.address}")


    def generate_map(self):
        """ Build maps and asdsociated markers for each ass mat"""

        for line in self.all_records:
            # get latitude/longitude from address
            ass_mat = AssMat(**line)
            self.total_ass_mat += 1
            ass_mat_coordinate = self.get_latitude_and_longitude_from_adress(ass_mat)
            if not ass_mat_coordinate:
                continue
            self.markers.append(
                folium.Marker(
                    ass_mat_coordinate,
                    popup=self.build_popup_message(ass_mat),
                    icon=self.choose_icon(ass_mat)
                )
            )



        self.duplicate_markers_on_same_location()
        self.maps.save('index.html')

    def duplicate_markers_on_same_location(self):
        """
        With general adress, two or more ass mat can have the same address
        It result on one markers for multiple ass mat
        slightly move markers if occurs
        """

        seen = []
        for marker in self.markers:
            location = marker.location[0],marker.location[1]
            if location not in seen:
                seen.append(location)
                marker.add_to(self.maps)
                continue

            logging.warning("Found two markers with the same location, change location point for visibility")
            marker.location[1] += 0.0001
            seen.append((marker.location[0],marker.location[1]))
            marker.add_to(self.maps)


    def open_in_browser(self):
        """open generate maps in default browser"""
        webbrowser.open_new_tab('index.html')

    def mapping_stats(self):
        logging.info(f"""
        Total ass mat = {self.total_ass_mat}
        Localized ass mat = {self.ass_mat_localized}
        Localization score : {self.ass_mat_localized*100/self.total_ass_mat}
        """)

if __name__ == '__main__':

    ass_mat_maps = AssMatMap()
    ass_mat_maps.generate_map()
    ass_mat_maps.open_in_browser()
    ass_mat_maps.mapping_stats()
